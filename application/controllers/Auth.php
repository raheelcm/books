<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */ 
	public function create()
	{
		$this->form_validation->set_rules('uname', 'User Name', 'required');
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('upass', 'Password', 'required');
        $this->form_validation->set_rules('cpass', 'Confirm Password', 'required');
        if ($this->form_validation->run() == FALSE)
        {
                $this->session->set_flashdata('error', validation_errors());
        }
        else
        {
        	$this->load->model('auth_model');
        	if($this->input->post('upass') != $this->input->post('cpass'))
        	{
        		$this->session->set_flashdata('error', "Password Not Match!");
        		header('Location: ' . $_SERVER['HTTP_REFERER']);
        	}
        	if($this->auth_model->get(array('uname'=>$this->input->post('uname'))))
        	{
        		$this->session->set_flashdata('error', "User Name Not Avalible");
        		header('Location: ' . $_SERVER['HTTP_REFERER']);
        		exit();
        	}
        	if($this->auth_model->get(array('email'=>$this->input->post('email'))))
        	{
        		$this->session->set_flashdata('error', "Email Already Exist");
        		header('Location: ' . $_SERVER['HTTP_REFERER']);
        		exit();
        	}

        	$arr = array(
        		"uname"=>$this->input->post('uname'),
        		"first_name"=>$this->input->post('first_name'),
        		"last_name"=>$this->input->post('last_name'),
        		"email"=>$this->input->post('email'),
        		"upass"=>md5($this->input->post('upass')),
        		"roleID"=>2,
        		"ip"=>$this->input->ip_address(),
        	);
        	if($bookID = $this->auth_model->create($arr))
        	{
        		
                $this->session->set_flashdata('success', 'Account Create successfully!');
                
        	}
        	else
        	{
        		$this->session->set_flashdata('error', 'server error');
        		
        	}
        }
        header('Location: ' . $_SERVER['HTTP_REFERER']);
exit;
		
	}
	public function register()
	{
		$data= array();
		$data['assets']= base_url('assets/books/');
		// die($data['assets']);
		$this->load->library('template');
		$this->template->front('register',$data);

		
	}
	public function post()
	{
		
				$this->form_validation->set_rules('uname', 'Username', 'required');
                
                $this->form_validation->set_rules('upass', 'Password', 'required');

                if ($this->form_validation->run() == FALSE)
                {
                		$this->session->set_flashdata('error', 'All fields required');

                }
                else
                { 
                	$uname = $this->input->post('uname');
                	$upass = $this->input->post('upass');
                	$this->load->model('auth_model');
                	$user = $this->auth_model->login($uname, $upass);
                	if($user->status == 1)
                	{
                		unset($user->upass);
	                	$roleID = $user->roleID;
                                if($roleID)
                                {


        	                	$role = $this->auth_model->getrolebyid($roleID);
                                }
                                else{
                                        $this->session->set_flashdata('error', 'system error');
                                        redirect($_SERVER['HTTP_REFERER']);
                                }
	                	
	                	
	                	$lgn = array();
	                	$lgn[$role->name.'_login'] = $user;
	                	$this->auth_model->updateuserbyid($user->userID, array("ip"=>$this->input->ip_address()));
	                	$this->session->set_userdata($lgn); 

                                redirect(base_url().'/books');
                                exit();
	                }
	                else
	                {
	                	$this->session->set_flashdata('error', 'Account Blocked!');
	                }
                	
                	
                    
                }
                redirect($_SERVER['HTTP_REFERER']);

	}
	public function logout()
        {
                session_destroy();
    unset($_SESSION['user_login']);
    redirect(base_url());
        }
        public function login()
	{
                
                if(isset($_SESSION['user_login']))
                {
                        redirect(base_url().'books');
                }
		$data= array();
                $data['assets']= base_url('assets/books/');
                // die($data['assets']);
                $this->load->library('template');
                $this->template->front('login',$data);

		
	}
}

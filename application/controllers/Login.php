<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function post()
	{
		
				$this->form_validation->set_rules('uname', 'Username', 'required');
                
                $this->form_validation->set_rules('upass', 'Password', 'required');

                if ($this->form_validation->run() == FALSE)
                {
                		$this->session->set_flashdata('error', 'All fields required');

                }
                else
                { 
                	$uname = $this->input->post('uname');
                	$upass = $this->input->post('upass');
                	$this->load->model('login_model');
                	$user = $this->login_model->login($uname, $upass);
                	if($user->status == 1)
                	{
                		unset($user->upass);
	                	$roleID = $user->roleID;
	                	$role = $this->login_model->getrolebyid($roleID);
	                	
	                	// print_r();
	                	$lgn = array();
	                	$lgn[$role->name.'_login'] = $user;
	                	$this->login_model->updateuserbyid($user->userID, array("ip"=>$this->input->ip_address()));
	                	$this->session->set_userdata($lgn); 
	                }
	                else
	                {
	                	$this->session->set_flashdata('error', 'Account Blocked!');
	                }
                	
                	
                    
                }
                redirect($_SERVER['HTTP_REFERER']);

	}
	public function index()
	{
		if(isset($_SESSION['admininstrator_login']))
		{
			redirect(('/admin/admin'));
		}
		$data= array();
		$data['assets']= base_url('assets/admin/');
		$this->load->library('template');
		$this->template->full('login',$data);

		
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {
	function __construct() {
        parent::__construct();
        
    }
	
	public function index()
	{
		if(isset($_SESSION['user_login']))
                {
                        redirect(base_url().'/books');
                }
		$data= array();
		$data['assets']= base_url('assets/books/');
		// die($data['assets']);
		$this->load->library('template');
		$this->template->full('home',$data);

		
	}
	public function page($page = '')
	{
		// die("OK");
		$data= array();
		$data['assets']= base_url('assets/books/');
		// die($data['assets']);
		$this->load->library('template');
		$this->template->front($page,$data);


	}
	public function addbook()
	{
		// die("OK");
		$data= array();
		$data['assets']= base_url('assets/books/');
		// die($data['assets']);
		$this->load->library('template');
		$this->template->front('addbook',$data);

		
	}
	public function addgroup()
	{
		// die("OK");
		$data= array();
		$data['assets']= base_url('assets/books/');
		// die($data['assets']);
		$this->load->library('template');
		$this->template->front('addgroup',$data);

		
	}
}

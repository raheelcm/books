<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Books extends CI_Controller {
	function __construct() {
        parent::__construct();

        if(!isset($_SESSION['user_login']))
		{
			redirect(('/'));
		}
		$this->load->library('template');
		$this->load->model('Book_model');
		$this->load->model('Group_model');
        $this->load->model('Tags_model');
    }
	
    public function saveTags()
    {
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');

        if ($this->form_validation->run() == FALSE)
        {
                $this->session->set_flashdata('error', validation_errors());
        }
        else
        {
            $user = $this->session->userdata('user_login');
            $arr = array(
                "name" => $this->input->post('name'),
                "type" => $this->input->post('type'),
                "userID" => $user->UserID
            );

            if($this->Tags_model->add($arr))
            {
                $this->session->set_flashdata('success', 'Tag add successfully!');
            }
            else
            {
                $this->session->set_flashdata('error', 'server error');
            }
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function deleteTags($id = 0)
    {
        if($id)
        {
            if($this->Tags_model->update($id, array('status'=> 1)))
            {
                $this->session->set_flashdata('success', 'Tag delete successfully!');
            }
            else
            {
                $this->session->set_flashdata('error', 'server error');
            }
        }
        redirect($_SERVER['HTTP_REFERER']);
    }
	
    public function tags()
	{
        
		$data= array();
        $user = $this->session->userdata('user_login');
        $data['data']= $this->Tags_model->get(array('userID'=>$user->UserID,'status'=>0));
		$data['assets']= base_url('assets/books/');		
		$this->template->front('tags',$data);

		
	}
    public function index()
    {
        
        $data= array();
        $data['assets']= base_url('assets/books/');
        
        $this->template->front('home',$data);

        
    }
	public function saveGroup()
	{
        $this->form_validation->set_rules('langID', 'Group Language', 'required');
        $this->form_validation->set_rules('grouptypeID', 'Group Type', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('description', 'Group Description', 'required');
        if(!isset($_FILES['groupImage']['name']) && $_FILES['groupImage']['name'])
        {
        	$this->form_validation->set_rules('groupImage', 'Group Image', 'required');
        }


        if ($this->form_validation->run() == FALSE)
        {
                $this->session->set_flashdata('error', validation_errors());
        }
        else
        {

        	$groupImage = 0;
        	if(isset($_FILES['groupImage']))
	        {
	        	$imgData = $this->template->upload('groupImage');
	        	$groupImage = $this->Group_model->addMedia($imgData);
	        	// die();
	        }
        	$user = $this->session->userdata('user_login');
        	$arr = array(
        		"name" => $this->input->post('name'),
        		"description" => $this->input->post('description'),
        		"langID" => $this->input->post('langID'),
        		"grouptypeID" => $this->input->post('grouptypeID'),
        		"groupImage" => $groupImage,
        		"userID" => $user->UserID
        	);
        	if($this->Group_model->add($arr))
        	{
                $this->session->set_flashdata('success', 'Group add successfully!');
        	}
        	else
        	{
        		$this->session->set_flashdata('error', 'server error');
        	}

        }
        redirect($_SERVER['HTTP_REFERER']);
	}
    private function upload_csv($name)
    {
        if(isset($_FILES[$name]))
            {
                
                 $target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES[$name]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(true) {
    $uploadOk = 1;
    if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["csv_file"]["tmp_name"], $target_file)) {
        return $target_file;
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
}
die("PK");

            }
    }
    public function savecsv()
    {
        if(!$_FILES['csv_file']['name'])
        {
            $this->form_validation->set_rules('csv_file', 'CSV File', 'required');
        }


        if (FALSE)
        {

                $this->session->set_flashdata('error', "Please select csv file");
        }
        else
        {
            if($_FILES['csv_file']['name'])
            {
                $name = $this->upload_csv('csv_file');
                $ext  = pathinfo($name, PATHINFO_EXTENSION);
                if($ext != "csv")
                {
                    $this->session->set_flashdata('error', "Only CSV Allowed");
                    redirect($_SERVER['HTTP_REFERER']);
                    exit();
                }
                $cdata = array();
                $file = fopen($name, 'r');
                while (($line = fgetcsv($file)) !== FALSE) {
                  //$line is an aray of the csv elements
                    $cdata[] = $line;
                }
                fclose($file);
                foreach ($cdata as $key => $value) {
                    if($key != 0)
                    {
                        $arr = array(
                "title" => $value[0],
                "typeID" => $value[3],
                "isbnNO" => $value[2],
                "discription" => $value[0],
                "authorID" => $author_id,
                "coverImg" => $mediaID,
                "uid" => (isset($user->userID))?$user->userID:'1'
            );
                    }
                }
            }
            
            
            $arr = array(
                "name" => $this->input->post('name'),
                "description" => $this->input->post('description'),
                "langID" => $this->input->post('langID'),
                "grouptypeID" => $this->input->post('grouptypeID'),
                "groupImage" => $groupImage,
                "userID" => $user->UserID
            );
            if($this->Group_model->add($arr))
            {
                $this->session->set_flashdata('success', 'Group add successfully!');
            }
            else
            {
                $this->session->set_flashdata('error', 'server error');
            }

        }
        redirect($_SERVER['HTTP_REFERER']);
    }
	public function save()
	{
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('typeID', 'Book type', 'required');
        $this->form_validation->set_rules('genres_id', 'Genres', 'required');
        $this->form_validation->set_rules('tags_id', 'Tags', 'required');
        $this->form_validation->set_rules('list_id', 'Language', 'required');
        $this->form_validation->set_rules('isbnNO', 'ISBN No', 'required');
        $this->form_validation->set_rules('discription', 'Discription', 'required');
        if(!isset($_FILES['book_image']))
        {
        	$this->form_validation->set_rules('book_image', 'Cover Photo', 'required');
        }

        if ($this->form_validation->run() == FALSE)
        {
                $this->session->set_flashdata('error', validation_errors());
        }
        else
        { 
        	$user = $this->session->userdata('user_login');
        	$author_id = 0;
        	$mediaID = 0;
        	
        	if(isset($_FILES['book_image']))
	        {
	        	$imgData = $this->template->upload('book_image');

	        	$mediaID = $this->Book_model->addMedia($imgData);	

	        }

        	if($this->input->post('author_id'))
        	{
        		$author_id = $this->input->post('author_id');

        	}
        	else
        	{
        		
        		if($this->input->post('author'))
        		{

        			// die($this->input->post('author'));
	        		$ar = array(
	        			"authorID"=>'',
	        			"name"=>$this->input->post('author'),
	        			"userID" => $user->UserID
	        		);
	        		$author_id = $this->Book_model->getAuthorID($ar);
	        		

	        	}
	        	else
	        	{
	        		$this->session->set_flashdata('error', "Please Fill author!");
	        		redirect($_SERVER['HTTP_REFERER']);
	        	}
        	}
        	
        	
        	$arr = array(
        		"title" => $this->input->post('title'),
        		"typeID" => $this->input->post('typeID'),
        		"isbnNO" => $this->input->post('isbnNO'),
        		"discription" => $this->input->post('discription'),
        		"authorID" => $author_id,
        		"coverImg" => $mediaID,
        		"uid" => (isset($user->userID))?$user->userID:'1'
        	);
        	if($bookID = $this->Book_model->add($arr))
        	{
        		$genres_id = $this->input->post('genres_id');
        		$genres_id = explode(',', $genres_id);
        		foreach ($genres_id as $key => $value) {
        			$this->Book_model->addBookGenre($bookID,$value);
        		}
        		$tags_id = $this->input->post('tags_id');
        		$tags_id = explode(',', $tags_id);
        		foreach ($tags_id as $key => $value) {
        			$this->Book_model->addBookTag($bookID,$value);
        		}
        		$list_id = $this->input->post('list_id');
        		$list_id = explode(',', $list_id);
        		foreach ($list_id as $key => $value) {
        			$this->Book_model->addBookLang($bookID,$value);
        		}
                $this->session->set_flashdata('success', 'Book add successfully!');
        	}
        	else
        	{
        		$this->session->set_flashdata('error', 'server error');
        	}
        }
        redirect($_SERVER['HTTP_REFERER']);
	}
	public function addbook()
	{
		// die("OK");
		$data= array();
		$this->load->model('Book_model');
		$data['generes']= $this->Book_model->getGenere(array('status'=>0));

		$data['tags']= $this->Book_model->getTag(array('status'=>0));
		$data['list']= $this->Book_model->getList(array());
		
		$data['scripts']= array(
			'https://code.jquery.com/jquery-1.12.4.js',
			'https://code.jquery.com/ui/1.12.1/jquery-ui.js',
			'http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js',
			'https://rawgit.com/select2/select2/master/dist/js/select2.js',
			base_url('assets/admin/pages/').'book.js',
		);
		$data['css']= array(
			'http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
		);
		$data['assets']= base_url('assets/books/');
		$this->template->front('addbook',$data);

		
	}
	public function addgroup()
	{
		$data= array();
		$data['list']= $this->Group_model->getList(array());
		$data['types']= $this->Group_model->getGrouptype(array());
		$data['assets']= base_url('assets/books/');
		$this->template->front('addgroup',$data);

		
	}
}

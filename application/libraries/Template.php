<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template {
        public function upload($name)
        {
                $currentDir = getcwd();
    $uploadDirectory = "/uploads/";

    $errors = []; // Store all foreseen and unforseen errors here

    $fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions

    $fileName = $_FILES[$name]['name'];
    $fileSize = $_FILES[$name]['size'];
    $fileTmpName  = $_FILES[$name]['tmp_name'];
    $fileType = $_FILES[$name]['type'];
    $fileExtension = strtolower(end(explode('.',$fileName)));
    $uploadPath = $currentDir . $uploadDirectory . basename($fileName); 
    if (isset($_FILES[$name])) {

        if (! in_array($fileExtension,$fileExtensions)) {
            $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
        }

        if ($fileSize > 2000000) {
            $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
        }

        if (empty($errors)) {
            $didUpload = move_uploaded_file($fileTmpName, $uploadPath);

            if ($didUpload) {
                
            } else {
                
            }
        } else {
            foreach ($errors as $error) {
                
            }
        }
    }

                        $ret = \Cloudinary\Uploader::upload($uploadPath, array());
                    if($ret)
                    {
                        return $arr = array(
                                "public_id"=>$ret['public_id'],
                                "url"=>$ret['url'],
                                "cloudinary"=>1,
                                "localPath"=>$uploadPath
                        );
                    }

        }
        public function full($view , $data)
        {
        	$CI =& get_instance();

        	// die("OK");
        	if(!isset($data['title']))
        	{
        		$data['title'] = 'Books share';
        	}
        	$CI->load->view('fpages/'.$view,$data);
        }
        public function admin($view , $data)
        {
                $CI =& get_instance();

                // die("OK");
                if(!isset($data['title']))
                {
                        $data['title'] = 'Books share';
                }
                $CI->load->view('includes/header',$data);
                $CI->load->view('admin/'.$view,$data);
                $CI->load->view('includes/footer',$data);
        }
        public function front($view , $data)
        {
                $CI =& get_instance();

                // die("OK");
                if(!isset($data['title']))
                {
                        $data['title'] = 'Books share';
                }
                $CI->load->view('includes/front-header',$data);
                $CI->load->view('books/'.$view,$data);
                $CI->load->view('includes/front-footer',$data);
        }
}
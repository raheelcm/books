<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?= $title ?></title>

    <link href="<?= $assets?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= $assets?>font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?= $assets?>css/animate.css" rel="stylesheet">
    <link href="<?= $assets?>css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="loginColumns animated fadeInDown">
        <div class="row">

            <div class="col-md-6">
                <h2 class="font-bold">Welcome To Admin Panel Of<br/> Share Your Book</h2>

                <p>
By sharing your paper books with others, you save money, save trees and read more books. Join our passionate community of book lovers, as we change the world and touch lives, one book at a time.

                </p>



            </div>
            <div class="col-md-6">
                <div class="ibox-content">
                    <form class="m-t" method="post" role="form" action="<?=base_url('/login/post')?>"> 
                        <?php $this->load->view('flash') ?>
                        <div class="form-group">
                            <input type="email" name="uname" class="form-control" placeholder="Username" required="">
                        </div>
                        <div class="form-group">
                            <input type="password" name="upass" class="form-control" placeholder="Password" required="">
                        </div>
                        <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                     
                    </form>
                  
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Copyright@ Share Your Book 2019
            </div>
        
        </div>
    </div>

</body>

</html>

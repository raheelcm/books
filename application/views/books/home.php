<div class="container">
		<div class="banner">
			<img src="<?php echo $assets; ?>images/Untitled-7.png">
			<div class="bannertext">
				<h3>Our Motto:</h3>
				<p>Read and Help People read. After All as Napolean Bonaparte said “Show me a family of readers, and I will show you the people who move the world.”</p>
			</div>
		</div>
		<div class="boxes">
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<div class="box">
						<img src="<?php echo $assets; ?>images/pro4.png">
						<strong><a href="#">New Additions</a></strong>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="box">
						<img src="<?php echo $assets; ?>images/pro5.png">
						<strong><a href="#">Find your Books <br>(By Genre)</a></strong>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="box">
						<img src="<?php echo $assets; ?>images/pro6.png">
						<strong><a href="#">Share Books</a></strong>
					</div>
				</div>
			</div>
		</div>
		<div class="smalltext">
			<img src="<?php echo $assets; ?>images/pro7.png">
			<h3>Latest Books</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
		</div>
		<div class="products">
			<div class="row">
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="new"><a href="#">NEW</a></div>
						<div class="sale"><a href="#">Sale</a></div>
						<div class="productig">
							<a href="#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="productig">
							<a href="#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="productig">
							<a href="#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="productig">
							<a href="#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="new"><a href="#">NEW</a></div>
						<div class="sale"><a href="#">Sale</a></div>
						<div class="productig">
							<a href="#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="productig">
							<a href="#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="productig">
							<a href="#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="productig">
							<a href="#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="new"><a href="#">NEW</a></div>
						<div class="sale"><a href="#">Sale</a></div>
						<div class="productig">
							<a href="#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="productig">
							<a href="#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="productig">
							<a href="#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="productig">
							<a href="#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
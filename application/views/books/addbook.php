<div class="container">
		<div class="addbooks">
			<div class="row">
			  	<div class="col-md-4 my-top-spacing">     
		            <div class="panel panel-default">
		                <div class="panel-heading">Import Books From CSV File</div>

		                <div class="panel-body">
		                    <form id="book_form" action="<?=base_url('books/savecsv');?>" enctype="multipart/form-data" method="post" class="form-horizontal">
		                    	<?php $this->load->view('flash'); ?>
		                    	
		                        <div class="form-group">
		                            <label for="csv_file" class="col-md-4 control-label">Select file</label>

		                            <div class="col-md-8">
		                                <input id="csv_file" type="file" class="form-control" name="csv_file" required="">

		                                                                    </div>
		                        </div>

		                        <div class="form-group">
		                            <div class="col-md-6 col-md-offset-4">
		                                <div class="checkbox text-danger">
		                                    <label>
		                                        <input type="checkbox" name="header" checked=""> File must have headers
		                                    </label>
		                                </div>
		                            </div>
		                        </div>

		                        <div class="form-group">
		                            <div class="col-md-8 col-md-offset-4">
		                                <button type="submit" class="btn btn-primary">
		                                    Load file
		                                </button>
		                            </div>
		                        </div>
		                        <div><strong>Headers</strong>: title,author,isbn,book_type,language</div>
		                    </form>
		                </div>
		            </div>
		        </div>
		        <div class="col-md-8 my-top-spacing">
		            <div class="panel panel-default">
		                <div class="panel-heading"><h3>Add New Book </h3></div>
		                <div class="panel-body">
		                	<form id="book_form" onsubmit="return front_form();" action="<?=base_url('books/save');?>" enctype="multipart/form-data" method="post" class="form-horizontal">
		                    	<?php $this->load->view('flash'); ?>
		                        <div class="form-group">
		                            <label name="title">Title:</label>
		                            <input id="title" name="title" class="form-control" data-parsley-required="" data-parsley-maxlength="255"> 
		                        </div>
		                        <div class="form-group">
		                            <label name="author">Author:</label>
		                            <input name="author" class="form-control" data-parsley-required="" data-parsley-maxlength="120">
		                        </div>
		                        <div class="ui-widget">
  <label for="birds">Author: </label>
  <input id="author" name="author"
  value="
<?php
if(isset($edit['authorID']))
{
echo $authorID = $CI->Book_model->getAuthorByID($edit['authorID'])->name;
}

?>">
  <input id="author_id" type="hidden" name="author_id" 
value="<?= (isset($edit['authorID']))?$edit['authorID']:''; ?>" 
  >
</div>
		                        <div class="form-group">                       
		                            <label name="book_type_id">Book type:</label>
		                                <label class="radio-inline"><input type="radio" id="genre_type_id" name="typeID" value="1" checked="" data-parsley-multiple="typeID">fiction</label>
		                                                                
		                                <label class="radio-inline"><input type="radio" id="genre_type_id" name="typeID" value="2" data-parsley-multiple="typeID">nonfiction</label>
		                        </div>
		                        <div class="form-group">
		                            <label name="genres">Genres:</label>                        
		                            <select class="form-control" name="genres" id="genres" multiple="multiple" >
		                            	<?php
    foreach ($generes as $key => $value) {
        ?>
        <option value="<?= $value['genreID']; ?>"><?= $value['name']; ?></option>
        
        <?php
    }
    ?>
		                            </select>
		                            <input type="hidden" name="genres_id" id="genres_id">
		                        </div> 
		                        <div class="form-group">
		                            <label name="tags">Tags:</label>                        
			                            	                            <select class="form-control" name="tags" id="tags" multiple="multiple" >
		                            	<?php
    foreach ($tags as $key => $value) {
        ?>
        <option value="<?= $value['tagID']; ?>"><?= $value['name']; ?></option>
        
        <?php
    }
    ?>
		                                                                </select>

		                                                                <input type="hidden" name="tags_id" id="tags_id">
		                        </div> 
		                        
		                        <div class="form-group">
		                            <label name="title">ISBN number:</label>
		                            <input id="" name="isbnNO" class="form-control" data-parsley-required="" data-parsley-maxlength="255"> 
		                        </div>
		                        <div class="form-group">
		                            <label name="language_id">Book language:</label>                        
		                            <select class="form-control" name="list" id="list" multiple="multiple" >
		                            	<?php
    foreach ($list as $key => $value) {
        ?>
        <option value="<?= $value['id']; ?>"><?= $value['value']; ?></option>
        
        <?php
    }
    ?>
		                                                        </select>
		                                                        <input type="hidden" name="list_id" id="list_id">
		                        </div>

		                        <div class="form-group">
		                            <label name="group_id">Group:</label>
		                            <select name="group_id" class="form-control">
		                                   
		                                    <!--if its a library group and the owner is logged in or if it's not a library group add the group to the dropdown list -->
		                                    <!--This is to prevent users from adding books to libraries where they are only members and not owners--> 
		                                                                                <option value="490" >shazim </option>
		                                                                                                        </select>
		                        </div> 

		                        <div class="form-group">
		                            <label name="description">Book description:</label>
		                            <textarea id="description" name="discription" title="Let people know what the book is about" placeholder="Long enough to provide value to others" rows="5" class="form-control" data-parsley-maxlength="5000" value="" ></textarea>
		                        </div>                         
		                        <div class="form-group">
		                            <label name="book_image">Image:</label>
		                            <input type="file" id="book_image" name="book_image" accept="image/png, image/jpeg, image/jpg" class="form-control">
		                        </div>
		                        
		                        <input type="submit" value="Add Book" class="btn btn-success btn-lg btn-block">
		                        
		                    </form>
		                </div>
		            </div>
		        </div>
			    
			</div>
        </div>
	</div>
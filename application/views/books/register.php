<div class="container">
		<div class="register">
			<div class="">
				<div class="formarea">
					<h2>REGISTER AT SHAREBOOKS</h2>
					<form method="post" class="form-inline" action="<?= base_url('auth/create') ?>">
						<?= $this->load->view('flash') ?>
						<div class="form-group">
					      	<input type="text" class="form-control" id="" placeholder="Username*" name="uname">
					    </div>
					    <div class="form-group">
					      	<input type="text" class="form-control" id="" placeholder="First Name*" name="first_name">
					    </div>
					    <div class="form-group">
					      	<input type="text" class="form-control" id="" placeholder="Last Name*" name="last_name">
					    </div>
					    <div class="form-group">
					      	<input type="email" class="form-control" id="" placeholder="Email*" name="email">
					    </div>
					    <div class="form-group">
					      	<input type="password" class="form-control" id="" placeholder="Password*" name="upass">
					    </div>
					    <div class="form-group">
					      	<input type="password" class="form-control" id="" placeholder="Confirm Password*" name="cpass">
					    </div>
					    <input type="submit" class="btn btn-default" value="Registere">
					</form>
				</div>
			</div>
		</div>	
		<div class="products">
			<h2>Latest Books</h2>
			<div class="row">
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="new"><a href="<?php echo $assets; ?>#">NEW</a></div>
						<div class="sale"><a href="<?php echo $assets; ?>#">Sale</a></div>
						<div class="productig">
							<a href="<?php echo $assets; ?>#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="<?php echo $assets; ?>#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="<?php echo $assets; ?>#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="productig">
							<a href="<?php echo $assets; ?>#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="<?php echo $assets; ?>#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="<?php echo $assets; ?>#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="productig">
							<a href="<?php echo $assets; ?>#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="<?php echo $assets; ?>#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="<?php echo $assets; ?>#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="productig">
							<a href="<?php echo $assets; ?>#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="<?php echo $assets; ?>#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="<?php echo $assets; ?>#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="new"><a href="<?php echo $assets; ?>#">NEW</a></div>
						<div class="sale"><a href="<?php echo $assets; ?>#">Sale</a></div>
						<div class="productig">
							<a href="<?php echo $assets; ?>#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="<?php echo $assets; ?>#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="<?php echo $assets; ?>#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="productig">
							<a href="<?php echo $assets; ?>#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="<?php echo $assets; ?>#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="<?php echo $assets; ?>#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="productig">
							<a href="<?php echo $assets; ?>#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="<?php echo $assets; ?>#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="<?php echo $assets; ?>#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="productig">
							<a href="<?php echo $assets; ?>#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="<?php echo $assets; ?>#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="<?php echo $assets; ?>#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="new"><a href="<?php echo $assets; ?>#">NEW</a></div>
						<div class="sale"><a href="<?php echo $assets; ?>#">Sale</a></div>
						<div class="productig">
							<a href="<?php echo $assets; ?>#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="<?php echo $assets; ?>#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="<?php echo $assets; ?>#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="productig">
							<a href="<?php echo $assets; ?>#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="<?php echo $assets; ?>#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="<?php echo $assets; ?>#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="productig">
							<a href="<?php echo $assets; ?>#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="<?php echo $assets; ?>#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="<?php echo $assets; ?>#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="productbox">
						<div class="productig">
							<a href="<?php echo $assets; ?>#"><img src="<?php echo $assets; ?>images/pro8.png"></a>
						</div>
						<div class="productDet">
							<a href="<?php echo $assets; ?>#">
								<strong>Micheal</strong>
								<h3>Jewels of Nizam</h3>
							</a>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<strong class="price"><a href="<?php echo $assets; ?>#">5.000 KWD</a></strong>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="container">
		<div class="register">
			<div class="">
				<div class="formarea">
					<h2>Contact  SHAREBOOKS</h2>
					<form class="form-inline" action="">
						<p>Got questions? We would love to hear from you. Use the contact form below</p>
						<div class="form-group">
					      	<input type="text" class="form-control" id="" placeholder="Name*" name="">
					    </div>
					    <div class="form-group">
					      	<input type="email" class="form-control" id="" placeholder="Email*" name="">
					    </div>
					    <div class="form-group">
					      	<input type="text" class="form-control" id="" placeholder="Subject*" name="">
					    </div>
					    <div class="form-group">
					      	<textarea id="message" name="message" class="form-control" data-parsley-required="" required="" data-parsley-maxlength="500" placeholder="Type your message here ..."></textarea>
					    </div>
					    
					    <input type="submit" class="btn btn-default" value="Send message">
					</form>
				</div>
			</div>
		</div>	
		
	</div>
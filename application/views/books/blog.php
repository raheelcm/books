<div class="services">
		<div class="container">
			<div class="row">
				<!-- Sidebar -->
				<div class="col-md-4 col-sm-4">
					<!-- aside -->
					<aside class="sidebar">
						<div class="widget">
							<ul class="go-widget with-bg">
								<li><a class="active" href="blogDetail.html">How to read more paper books for free legally</a></li>
								<li><a href="#">35 quotes that will motivate you to achieve your reading goals</a></li>
								<li><a href="#">The only 3 tips you need to reach your reading challenge goal</a></li>
								<li><a href="#">How to build a local community of readers</a></li>
							</ul>
						</div>
						
						<!-- Widget -->
						<div class="widget box callto-action-widget">
							<div class="callto-action-wrap">
								<h5 class="title">Need Our Help?</h5>
								<p>Have a finance problem? Reach to us to get free advisor from our finance experts today.</p>
								<a class="btn" href="contactUs.html">Contact Now</a>
							</div>
							
						</div>
						
						
					</aside><!-- aside -->	
				</div><!-- Column -->
				
				<!-- Page Content -->
				<div class="col-md-8 col-sm-8">
					<!-- List row -->
					<ul class="row">
						<!-- Service Column -->
						<li class="col-xs-12 service-list-wrap">
							<!-- Service Wrapper -->
							<div class="row">
								<!-- Service Image Wrapper -->
								<div class="col-sm-5">
									<div class="service-img-wrap">
										<img alt="Service" class="img-responsive" src="images/service-01.jpg" width="600" height="220">
									</div>
								</div><!-- Service Image Wrapper -->
								<!-- Service Detail Wrapper -->
								<div class="col-sm-7">
									<div class="service-details">
										<h4><a href="#">How to read more paper books for free legally</a></h4>
										<p>“Books should go where they will be most appreciated and not sit unread.”― Christopher Paolini    Let me guess. When you read the title of</p>
										<a href="blogDetail.html" class="btn">Read more</a>
									</div><!-- Service details -->
								</div><!-- Service details -->
							</div><!-- Service detail Wrapper -->
							<!-- Divider -->
							<hr class="md">
						</li><!-- Column -->
						
						<!-- Service Column -->
						<li class="col-xs-12 service-list-wrap">
							<!-- Service Wrapper -->
							<div class="row">
								<!-- Service Image Wrapper -->
								<div class="col-sm-5">
									<div class="service-img-wrap">
										<img alt="Service" class="img-responsive" src="images/service-02.jpg" width="600" height="220">
									</div>
								</div><!-- Service Image Wrapper -->
								<!-- Service Detail Wrapper -->
								<div class="col-sm-7">
									<div class="service-details">
										<h4><a href="#">35 quotes that will motivate you to achieve your reading goals</a></h4>
										<p>We all have plans to achieve many goals. However, life is not perfect so we do not always get to tick off all goals from</p>
										<a href="#" class="btn">Read more</a>
									</div><!-- Service details -->
								</div><!-- Service details -->
							</div><!-- Service detail Wrapper -->
							<!-- Divider -->
							<hr class="md">
						</li><!-- Column -->
						
						<!-- Service Column -->
						<li class="col-xs-12 service-list-wrap">
							<!-- Service Wrapper -->
							<div class="row">
								<!-- Service Image Wrapper -->
								<div class="col-sm-5">
									<div class="service-img-wrap">
										<img alt="Service" class="img-responsive" src="images/service-03.jpg" width="600" height="220">
									</div>
								</div><!-- Service Image Wrapper -->
								<!-- Service Detail Wrapper -->
								<div class="col-sm-7">
									<div class="service-details">
										<h4><a href="#">The only 3 tips you need to reach your reading challenge goal</a></h4>
										<p>“To think is easy. To act is difficult. To act as one thinks is the most difficult.”&#8202; – &#8202;Johann Wolfgang Von Goeth   Making new</p>
										<a href="#" class="btn">Read more</a>
									</div><!-- Service details -->
								</div><!-- Service details -->
							</div><!-- Service detail Wrapper -->
							<!-- Divider -->
							<hr class="md">
						</li><!-- Column -->
						
						<!-- Service Column -->
						<li class="col-xs-12 service-list-wrap">
							<!-- Service Wrapper -->
							<div class="row">
								<!-- Service Image Wrapper -->
								<div class="col-sm-5">
									<div class="service-img-wrap">
										<img alt="Service" class="img-responsive" src="images/service-04.jpg" width="600" height="220">
									</div>
								</div><!-- Service Image Wrapper -->
								<!-- Service Detail Wrapper -->
								<div class="col-sm-7">
									<div class="service-details">
										<h4><a href="#">How to build a local community of readers</a></h4>
										<p>Book lovers have big plans. Plans to read more books, meet other book lovers, build a community of local book lovers or finish that book</p>
										<a href="#" class="btn">Read more</a>
									</div><!-- Service details -->
								</div><!-- Service details -->
							</div><!-- Service detail Wrapper -->
							<hr class="md">
						</li><!-- Column -->
						<!-- Service Column -->
						


					</ul><!-- List row -->
				</div><!-- Column -->
			</div><!-- Row -->
		</div>
	</div>
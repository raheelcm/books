<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>ALL Books</h2>
	</div>
	<div class="col-lg-2">

	</div>
</div>
<div class="row">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Find All Books </h5>
			</div>
			<div class="ibox-content" style="display: block;">
				<?php $this->load->view('flash') ?>
				<table class="footable table table-stripped toggle-arrow-tiny tablet breakpoint footable-loaded">
					<thead>
					<tr>

						<th data-toggle="true" class="footable-visible footable-first-column footable-sortable">Book Cover<span class="footable-sort-indicator"></span></th>
						<th data-toggle="true" class="footable-visible footable-first-column footable-sortable">Book Title<span class="footable-sort-indicator"></span></th>
						<th class="footable-visible footable-sortable">Author<span class="footable-sort-indicator"></span></th>
						<th class="footable-visible footable-sortable">ISBN Number<span class="footable-sort-indicator"></span></th>
						<th data-hide="all" class="footable-sortable">Genre<span class="footable-sort-indicator"></span></th>

						<th data-hide="all" class="footable-sortable">Ratings<span class="footable-sort-indicator"></span></th>
						<th data-hide="all" class="footable-sortable">Language<span class="footable-sort-indicator"></span></th>
						<th data-hide="all" class="footable-sortable" >Actions<span class="footable-sort-indicator"></span></th>
						<th data-hide="all" class="footable-sortable" style="display: none;">Date<span class="footable-sort-indicator"></span></th>
					</tr>
					</thead>
					<tbody>
						<?php

						foreach ($data as $key => $value) {
							$CI = get_instance();
							$coverImg = $CI->Book_model->getMediaByID($value['coverImg']);
							$authorID = $CI->Book_model->getAuthorByID($value['authorID']);
							$genres = $CI->Book_model->getGenreByBookID($value['bookID']);
							$lang = $CI->Book_model->getLangByBookID($value['bookID']);
						?>


					<tr class="footable-even" style="display: table-row;">
						<td class="footable-visible footable-first-column"><span class="footable-toggle"></span>
							<img src="<?= $coverImg->url ?>" width="80px" height="auto"></td>
						<td class="footable-visible"><?= $value['title'] ?></td>
						<td class="footable-visible"><?= $authorID->name ?></td>
						<td class="footable-visible"><?=$value['isbnNO']?></td>
						<td class="footable-visible">
							<?php
							foreach ($genres as $key => $lng) {
								if($key == 0)
								{
									echo $lng['name'];
								}
								else
								{
									echo ','.$lng['name'];
								}
							}
							?>
						</td>
						<td class="footable-visible"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></td>
						<td class="footable-visible">
							<?php
							foreach ($lang as $key => $lng) {
								if($key == 0)
								{
									echo $lng['name'];
								}
								else
								{
									echo ','.$lng['name'];
								}
							}
							?>
						</td>
						<td class="footable-visible">
							<p><button class="btn btn-sm btn-primary pull-right" style="width:100%; margin-bottom:10px;" type="submit"><strong>View</strong></button></p>
							<p><a  href="<?php echo base_url('admin/book/edit/').$value['bookID']; ?>" class="btn btn-sm btn-primary pull-right m-t-n-xs" style="width:100%" type="submit"><strong>Edit</strong></a </p>
							<p><a  href="<?php echo base_url('admin/book/delete/').$value['bookID']; ?>" class="btn btn-sm btn-primary pull-right m-t-n-xs" style="width:100%" type="submit"><strong>Delete</strong></a </p>
						</td>
					</tr>
					<?php
						}
						?>




					</tbody>
					<tfoot>
					<tr>
						<td colspan="5" class="footable-visible">
							<ul class="pagination pull-right"><li class="footable-page-arrow disabled"><a data-page="first" href="#first">«</a></li><li class="footable-page-arrow disabled"><a data-page="prev" href="#prev">‹</a></li><li class="footable-page active"><a data-page="0" href="#">1</a></li><li class="footable-page"><a data-page="1" href="#">2</a></li><li class="footable-page-arrow"><a data-page="next" href="#next">›</a></li><li class="footable-page-arrow"><a data-page="last" href="#last">»</a></li></ul>
						</td>
					</tr>
					</tfoot>
				</table>

			</div>
		</div>
	</div>

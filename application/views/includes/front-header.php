<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= $title ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo $assets; ?>css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $assets; ?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo $assets; ?>css/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $assets; ?>css/owl.theme.default.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $assets; ?>css/style.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.css" integrity="sha256-kalgQ55Pfy9YBkT+4yYYd5N8Iobe+iWeBuzP7LjVO0o=" crossorigin="anonymous" />
	    <?php
    if(isset($css) && is_array($css))
    {
        foreach ($css as $key => $value) {
           ?>
           <link href="<?php echo $value; ?>"/>
           <?php
        }

    }    
?>
</head>
<body>
<div id="main">
	<header id="header">
		<div class="container">
			<div class="headwithlogo">
				<div id="logo"><a href="<?php echo $assets; ?>index.html"><img src="<?php echo $assets; ?>images/logo.png"></a></div>
				<div class="search">
					<form class="form-inline" action="">
						<div class="form-group">
					      	<input type="text" class="form-control" id="" placeholder="Search...Title / Author / ISBN number" name="">
					    </div>
					    <input type="submit" class="btn btn-default" value="Submit">
					</form>
				</div>
			</div>
		</div>
		<div class="mainmenu">
			<div class="navbar-header">
	          	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"><i class="fa fa-bars"></i></button>
	        </div>
	        <?= $this->load->view('includes/menu') ?>
		</div>
	</header>
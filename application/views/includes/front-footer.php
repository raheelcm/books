<footer id="footer">
	<div class="row">
		<div class="col-md-4 col-sm-4">
			<div class="footer-menu">
				<ul>
					<li class="active"><a href="<?php echo $assets; ?>index.html">Home<i class="fa fa-chevron-right"></i></a></li>
					<li><a href="<?php echo $assets; ?>#">About us<i class="fa fa-chevron-right"></i></a></li>
					<li><a href="<?php echo $assets; ?>#">Login<i class="fa fa-chevron-right"></i></a></li>
					<li><a href="<?php echo $assets; ?>#">FAQ<i class="fa fa-chevron-right"></i></a></li>
					<li><a href="<?php echo $assets; ?>#">Contact Us<i class="fa fa-chevron-right"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="col-md-4 col-sm-4">
			<div class="newsletter">
				<h2>Our Newsletter</h2>
				<p>Sign up for our mailing list to get latest updates.</p>
				<form class="form-inline" action="subscribe.html">
					<div class="form-group">
						<input type="email" class="form-control" id="" placeholder="Enter Your Email" name="">
					</div>
					<div class="newsbtn"><input type="submit" class="btn btn-default" value="SUBSCRIBE"></div>
				</form>

			</div>
		</div>
		<div class="col-md-4 col-sm-4">
			<div class="instagram">
				<h2>instagram</h2>
				<a href="<?php echo $assets; ?>#"><img src="<?php echo $assets; ?>images/pro15.png"></a>
			</div>
		</div>
	</div>
	<div class="footerbottom">
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<div class="footerbottombox">
					<a href="<?php echo $assets; ?>#"><i class="fa fa-phone"></i></a>
					<p>Call us Now :</p>
					<a href="<?php echo $assets; ?>#">(+800) 0321-765-986</a>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="footerbottombox center">
					<a href="<?php echo $assets; ?>#"><i class="fa fa-home"></i></a>
					<p>Address:</p>
					<a>Flat 606 Aisha Al Salem complex Beind Al Gar Kuwait City kuwait 12170.</a>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="footerbottombox ">
					<a href="<?php echo $assets; ?>#"><i class="fa fa-envelope"></i></a>
					<p>Email :</p>
					<a href="<?php echo $assets; ?>#">Info@shareyourbooks.org</a>
				</div>
			</div>
		</div>
	</div>
</footer>
</div>
<script type="text/javascript" src="<?php echo $assets; ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $assets; ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $assets; ?>js/owl.carousel.min.js"></script>
<script type="text/javascript">
    var base_url = "<?= base_url(); ?>";
</script>
<?php
if(isset($scripts) && is_array($scripts))
{
	foreach ($scripts as $key => $value) {
		?>
		<script src="<?php echo $value; ?>"></script>
		<?php
	}

}
?>
</html>

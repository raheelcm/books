<div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="<?php echo base_url('/');; ?>img/profile_small.jpg" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="<?php echo base_url('/'); ?>#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"> Sumeet Rattan</strong>
                             </span> <span class="text-muted text-xs block">Owner / Admin 
                               <!-- <b class="caret"></b>--></span> </span> </a>
                       <!-- <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="<?php //echo base_url('/');; ?>profile.html">Profile</a></li>
                            <li><a href="<?php //echo base_url('/');; ?>contacts.html">Contacts</a></li>
                            <li><a href="<?php //echo base_url('/');; ?>mailbox.html">Mailbox</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php //echo base_url('/');; ?>login.html">Logout</a></li>
                        </ul>-->
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
                <li>
                    <a href=""><i class="fa fa-book"></i> <span class="nav-label">Books</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo base_url('/admin/book/all'); ?>">All Books</a></li>
                        <li><a href="<?php echo base_url('/admin/book/create'); ?>">Add New Book</a></li>
                    </ul>
                </li>
                <li>

                 
                <li>
                    <a href="<?php echo base_url('/');; ?>#"><i class="fa fa-tag"></i> <span class="nav-label">Tags</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo base_url('/admin/tags/all'); ?> ">All Tags</a></li>
                        <li><a href="<?php echo base_url('/admin/tags/create'); ?>">Add New Tag</a></li>
                    </ul>
                </li>
                <li>    

                <li>
                    <a href="<?php echo base_url('/'); ?>#"><i class="fa fa-group"></i> <span class="nav-label">Groups</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo base_url('/'); ?> ">New Group Requests</a></li>
                        <li><a href="<?php echo base_url('/admin/group/all'); ?>">All Groups</a></li>
                        <li><a href="<?php echo base_url('/admin/group/create'); ?>">Add New Group</a></li>
                    </ul>
                </li>


                <li>
                    <a href="<?php echo base_url('/');; ?>#"><i class="fa fa-edit"></i> <span class="nav-label">Genres</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo base_url('/admin/genres/all'); ?>">All Genres</a></li>
                        <li><a href="<?php echo base_url('/admin/genres/create'); ?>">Add New Genres</a></li>
                    </ul>
                </li>

                <li>
                    <a href="<?php echo base_url('/'); ?>layouts.html"><i class="fa fa-book"></i> <span class="nav-label">Borrow Books</span></a>
                </li>
                   <li>
                    <a href="<?php echo base_url('/'); ?>layouts.html"><i class="fa fa-paper-plane-o"></i> <span class="nav-label">Lent Books</span></a>


                </li>
                
               
                <li>    
                    <a href="<?php echo base_url('/');; ?>layouts.html"><i class="fa fa-group"></i> <span class="nav-label">Users</span></a>
                </li> 
               
               
            </ul>

        </div>
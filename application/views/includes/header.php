<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?= $title;?></title>

    <link href="<?php echo $assets; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $assets; ?>font-awesome/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <style>
  .ui-autocomplete-loading {
    background: white url("images/ui-anim_basic_16x16.gif") right center no-repeat;
  }
  </style>
    <?php
    if(isset($css) && is_array($css))
    {
        foreach ($css as $key => $value) {
           ?>
           <link href="<?php echo $value; ?>"/>
           <?php
        }

    }    
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.css" integrity="sha256-kalgQ55Pfy9YBkT+4yYYd5N8Iobe+iWeBuzP7LjVO0o=" crossorigin="anonymous" />

    <link href="<?php echo $assets; ?>css/animate.css" rel="stylesheet">
    <link href="<?php echo $assets; ?>css/style.css" rel="stylesheet">
    <link href="<?php echo $assets; ?>css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="<?php echo $assets; ?>css/plugins/summernote/summernote-bs3.css" rel="stylesheet">

</head>

<body>
    <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <?=$this->load->view('includes/sidebar')?>
    </nav>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="<?php echo $assets; ?>#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Welcome To ShareYourBook.org  Admin Panel.</span>
                </li>
                              
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="<?php echo $assets; ?>#">
                        <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="<?php echo $assets; ?>mailbox.html">
                                <div>
                                    <i class="fa fa-group"></i>  6 New Group Requests to Approve
                                </div>
                            </a>
                        </li>
                       
                    </ul>
                </li>


                <li>
                    <a href="<?php echo $assets; ?>login.html">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
                
            </ul>

        </nav>
        </div>
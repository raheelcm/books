$( function() {
    function log( message ) {
      $( "<div>" ).text( message ).prependTo( "#log" );
      $( "#log" ).scrollTop( 0 );
    }
 
    $( "#author" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: base_url+"admin/book/aSearch",
          dataType: "jsonp",
          data: {
            term: request.term
          },
          success: function( data ) {
            console.log(data);
            response( data );
          }
        } );
      },
      minLength: 2,
      select: function( event, ui ) {
      	$('#author_id').val(ui.item.id);
      }
    } );
  } );
$('#genres').select2({
    placeholder: 'Select a genre'
});
$('#tags').select2({
    placeholder: 'Select a tag'
});
$('#list').select2({
    placeholder: 'Select a Language'
});
function front_form()
{
  $('#genres_id').val($('#genres').val());
  // alert($('#tags').val());
  $('#tags_id').val($('#tags').val());
  $('#list_id').val($('#list').val());
  return true;
}
$("#book_form").submit(function(){
  $('#genres_id').val($('#genres').val());
  // alert($('#tags').val());
  $('#tags_id').val($('#tags').val());
  $('#list_id').val($('#list').val());
});